#!/bin/bash
export XILINX_BNN_ROOT=/opt/$PYNQ_PYTHON/lib/$PYNQ_PYTHON/site-packages/bnn/src
export VIVADOHLS_INCLUDE_PATH=/usr/include/vivado

_NETWORK=cnv-pynq
_PLATFORM=python_hw

cd $XILINX_BNN_ROOT/network/
./make-sw.sh $_NETWORK $_PLATFORM

cp --verbose "$XILINX_BNN_ROOT/network/output/sw/${_PLATFORM}-${_NETWORK}.so" "$XILINX_BNN_ROOT/../libraries/python_hdmi_bnn-${_NETWORK}.so"
sync
