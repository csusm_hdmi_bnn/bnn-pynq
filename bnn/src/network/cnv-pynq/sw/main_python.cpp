/******************************************************************************
 *  Copyright (c) 2016, Xilinx, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1.  Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  2.  Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 *  3.  Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/
/******************************************************************************
 *
 *
 * @file main_python.c
 *
 * Host code for BNN, overlay CNV-Pynq, to manage parameter loading, 
 * classification (inference) of single and multiple images
 * 
 *
 *****************************************************************************/

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <iostream>
#include <string.h>
#include <vector>

#include "foldedmv-offload.h"
#include "tiny_cnn/tiny_cnn.h"
#include "tiny_cnn/util/util.h"

using namespace std;
using namespace tiny_cnn;
using namespace tiny_cnn::activation;


void makeNetwork(network<mse, adagrad> & nn) {
  nn
#ifdef OFFLOAD
      << chaninterleave_layer<identity>(3, 32*32, false)
      << offloaded_layer(3*32*32, 10, &FixedFoldedMVOffload<8, 1>, 0xdeadbeef, 0)
#endif
      ;
}



extern "C" void load_parameters(const char* path)
{
#include "config.h"
FoldedMVInit("cnv-pynq");
network<mse, adagrad> nn;
makeNetwork(nn);
        cout << "Loading network parameters: " << path << endl;
        FoldedMVLoadLayerMem(path , 0, L0_PE, L0_WMEM, L0_TMEM);
        FoldedMVLoadLayerMem(path , 1, L1_PE, L1_WMEM, L1_TMEM);
        FoldedMVLoadLayerMem(path , 2, L2_PE, L2_WMEM, L2_TMEM);
        FoldedMVLoadLayerMem(path , 3, L3_PE, L3_WMEM, L3_TMEM);
        FoldedMVLoadLayerMem(path , 4, L4_PE, L4_WMEM, L4_TMEM);
        FoldedMVLoadLayerMem(path , 5, L5_PE, L5_WMEM, L5_TMEM);
        FoldedMVLoadLayerMem(path , 6, L6_PE, L6_WMEM, L6_TMEM);
        FoldedMVLoadLayerMem(path , 7, L7_PE, L7_WMEM, L7_TMEM);
        FoldedMVLoadLayerMem(path , 8, L8_PE, L8_WMEM, L8_TMEM);
}

extern "C" unsigned int inference(const char* path, unsigned int results[64], int number_class, float *usecPerImage)
{

FoldedMVInit("cnv-pynq");

network<mse, adagrad> nn;

makeNetwork(nn);
std::vector<label_t> test_labels;
std::vector<vec_t> test_images;

parse_cifar10(path, &test_images, &test_labels, -1.0, 1.0, 0, 0);
std::vector<unsigned int> class_result;
float usecPerImage_int;
class_result=testPrebuiltCIFAR10_from_image<8, 16>(test_images, number_class, usecPerImage_int);
if(results)
	std::copy(class_result.begin(),class_result.end(), results);
if (usecPerImage)
    *usecPerImage = usecPerImage_int;
return (std::distance(class_result.begin(),std::max_element(class_result.begin(), class_result.end())));
}

extern "C" unsigned int* inference_multiple(const char* path, int number_class, int *image_number, float *usecPerImage, unsigned int enable_detail = 0)
{

FoldedMVInit("cnv-pynq");

network<mse, adagrad> nn;

makeNetwork(nn);

std::vector<label_t> test_labels;
std::vector<vec_t> test_images;

parse_cifar10(path,
                      &test_images, &test_labels, -1.0, 1.0, 0, 0);
std::vector<unsigned int> all_result;
std::vector<unsigned int> detailed_results;
float usecPerImage_int;
all_result=testPrebuiltCIFAR10_multiple_images<8, 16>(test_images, number_class, detailed_results, usecPerImage_int);
unsigned int * result;
if (image_number)
   *image_number = all_result.size();
if (usecPerImage)
    *usecPerImage = usecPerImage_int;
if (enable_detail)
{
	result = new unsigned int [detailed_results.size()];
	std::copy(detailed_results.begin(),detailed_results.end(), result);
}
else
{
	result = new unsigned int [all_result.size()];
	std::copy(all_result.begin(),all_result.end(), result);
}
   
return result;
}


namespace {
	const unsigned int imageColorChannels = 3;  // R, G, B

	const unsigned int tileWidth = 32;
	const unsigned int tileHeight = 32;
	const unsigned int tileArea = tileWidth * tileHeight;


	inline tiny_cnn::float_t scale_color_byte(
		const unsigned char colorByte)
	{
		static const tiny_cnn::float_t scaleMin = -1.0;
		static const tiny_cnn::float_t scaleMax = 1.0;

		return scaleMin + (scaleMax - scaleMin) * colorByte / 0xFF;
	}


	void copy_tile_from_image(
		const unsigned char * const imageData,
		const unsigned int imageWidth,
		tiny_cnn::vec_t &tile,
		const unsigned int tileLeft,
		const unsigned int tileTop)
	{
		const unsigned int tileRight{tileLeft + tileWidth - 1};
		const unsigned int tileBottom{tileTop + tileHeight - 1};

		unsigned int pixelIndex{0};  // Index within this tile

		// Iterate tile pixels from left to right, top to bottom
		for (unsigned int pixelY{tileTop}; pixelY <= tileBottom; ++pixelY) {
			for (unsigned int pixelX{tileLeft}; pixelX <= tileRight; ++pixelX) {

				const unsigned char * const pixel{
					&imageData[(pixelY * imageWidth + pixelX) * imageColorChannels]};

				const tiny_cnn::float_t r{scale_color_byte(pixel[0])};
				const tiny_cnn::float_t g{scale_color_byte(pixel[1])};
				const tiny_cnn::float_t b{scale_color_byte(pixel[2])};

				// Tile data is sorted by color: All red values, then blues, then greens
				tile[0 * tileArea + pixelIndex] = r;
				tile[1 * tileArea + pixelIndex] = g;
				tile[2 * tileArea + pixelIndex] = b;

				++pixelIndex;
			}
		}
	}


	void split_image_tiles(
		const unsigned char * const imageData,
		const unsigned int imageWidth,
		std::vector<tiny_cnn::vec_t> &gridTiles,
		const unsigned int gridLeft,
		const unsigned int gridTop,
		const unsigned int gridColumns,
		const unsigned int gridRows)
	{
		// Iterate over grid tiles from left to right, top to bottom
		// Warning: Accessing tile-by-tile like this results in lots of cache misses!
		for (unsigned int tileY{0}, tileTop{gridTop};
			tileY < gridRows;
			++tileY, tileTop += tileHeight)
		{
			for (unsigned int tileX{0}, tileLeft{gridLeft};
				tileX < gridColumns;
				++tileX, tileLeft += tileWidth)
			{
				tiny_cnn::vec_t &tile{gridTiles[tileY * gridColumns + tileX]};

				copy_tile_from_image(
					imageData, imageWidth,
					tile, tileLeft, tileTop);
			}
		}
	}


	void outline_rectangle(
		unsigned char * const imageData,
		const unsigned int imageWidth,
		const unsigned char * const color,
		const unsigned int left,
		const unsigned int right,
		const unsigned int top,
		const unsigned int bottom)
	{
		if (left > right || top > bottom) {
			return;  // Draw nothing
		}

		// Top and bottom borders
		{
			unsigned char *imageDataTop{&imageData[
				(top * imageWidth + left) * imageColorChannels]};
			unsigned char *imageDataBottom{
				imageDataTop + (bottom - top) * imageWidth * imageColorChannels};

			for (unsigned int x{left}; x <= right; ++x) {
				for (unsigned int colorIndex{0}; colorIndex < imageColorChannels; ++colorIndex) {

					const unsigned char colorByte{color[colorIndex]};
					*(imageDataTop++) = colorByte;
					*(imageDataBottom++) = colorByte;
				}
			}
		}

		// Left and right borders
		{
			const unsigned int rowMinusPixelIncrement{(imageWidth - 1) * imageColorChannels};

			unsigned char *imageDataLeft{&imageData[
				(top * imageWidth + left) * imageColorChannels]};
			unsigned char *imageDataRight{
				imageDataLeft + (right - left) * imageColorChannels};

			for (unsigned int y{top}; y <= bottom; ++y) {
				for (unsigned int colorIndex{0}; colorIndex < imageColorChannels; ++colorIndex) {

					const unsigned char colorByte{color[colorIndex]};
					*(imageDataLeft++) = colorByte;
					*(imageDataRight++) = colorByte;
				}

				imageDataLeft += rowMinusPixelIncrement;
				imageDataRight += rowMinusPixelIncrement;
			}
		}
	}


	void shade_rectangle(
		unsigned char * const imageData,
		const unsigned int imageWidth,
		const unsigned char * const shadingColor,
		float shadingAlpha,
		const unsigned int left,
		const unsigned int right,
		const unsigned int top,
		const unsigned int bottom)
	{
		if (left > right || top > bottom
			|| shadingAlpha <= 0.0f)
		{
			return;  // Draw nothing
		}

		shadingAlpha = std::min(1.0f, shadingAlpha);
		const float backgroundAlpha = 1 - shadingAlpha;

		// Pre-multiply transparent foreground color
		const float foregroundColor[imageColorChannels]{
			shadingColor[0] * shadingAlpha,
			shadingColor[1] * shadingAlpha,
			shadingColor[2] * shadingAlpha};

		for (unsigned int y{top}; y <= bottom; ++y) {
			for (unsigned int x{left}; x <= right; ++x) {

				unsigned char * const pixel{
					&imageData[(y * imageWidth + x) * imageColorChannels]};

				for (unsigned int colorIndex{0}; colorIndex < imageColorChannels; ++colorIndex) {
					pixel[colorIndex] = static_cast<unsigned char>(0.5f  // Round
						+ foregroundColor[colorIndex]
						+ pixel[colorIndex] * backgroundAlpha);
				}
			}
		}
	}
}


/** @brief Classifies a grid of tiles within @p imageData, and highlights matching tiles.
@param[inout] imageData  Numpy array data buffer, in row-column-RGB order.
@param[in] imageWidth  Number of pixel columns within @p imageData.
@param[in] imageHeight  Number of pixel rows within @p imageData.
@param[in] gridLeft  Rightwards pixel offset for the left-most tiles in the grid.
@param[in] gridTop  Downwards pixel offset for the top-most tiles in the grid.
@param[in] gridColumns  Number of tile grid columns to classify.
@param[in] gridRows  Number of tile grid rows to classify.
@param[in] numCategories  How many categories the current network parameters are meant to classify into.
@param[in] targetCategoryId  The category ID to look for and highlight wihin the image.
@param[in] highlightThreshold  The minimum classification result required to highlight a match.
@param[in] matchThreshold  The minimum classification result required to count as a match.
@return The number of tiles matching above @p matchThreshold.
*/
extern "C" unsigned int inference_highlight_tiles(
	unsigned char * const imageData,
	const unsigned int imageWidth,
	const unsigned int imageHeight,
	const unsigned int gridLeft,
	const unsigned int gridTop,
	unsigned int gridColumns,
	unsigned int gridRows,
	const unsigned int numCategories,
	const unsigned int targetCategoryId,
	const float highlightThreshold,
	const float matchThreshold)
{
	static std::vector<tiny_cnn::vec_t> gridTiles;

	// Clamp tile grid size to screen resolution
	gridColumns = std::min(gridColumns, (imageWidth - gridLeft) / tileWidth);
	gridRows = std::min(gridRows, (imageHeight - gridTop) / tileHeight);
	if (!gridColumns || !gridRows) {
		return 0.0f;  // No tiles to classify
	}

	// Resize pre-allocated tile memory
	gridTiles.resize(gridColumns * gridRows,
		tiny_cnn::vec_t(static_cast<std::size_t>(tileArea * imageColorChannels)));

	split_image_tiles(
		imageData, imageWidth,
		gridTiles, gridLeft, gridTop, gridColumns, gridRows);

	FoldedMVInit("cnv-pynq");
	network<mse, adagrad> nn;
	makeNetwork(nn);

	float usecPerImage;
	std::vector<unsigned int> resultsDetailed;
	testPrebuiltCIFAR10_multiple_images<8, 16>(
		gridTiles, numCategories, resultsDetailed, usecPerImage);

	// Highlight matching tiles
	unsigned int matchCount{0};
	unsigned int tileIndex{0};
	for (unsigned int tileRow{0}, tileTop{gridTop};
		tileRow < gridRows;
		++tileRow, tileTop += tileHeight)
	{
		for (unsigned int tileColumn{0}, tileLeft{gridLeft};
			tileColumn < gridColumns;
			++tileColumn, tileLeft += tileWidth)
		{
			const unsigned int tileResultTarget{resultsDetailed[
				tileIndex * numCategories + targetCategoryId]};
			// Adjust to range between -1.0 (certainly not a match) to 1.0 (certain of a match)
			const float matchConfidence{tileResultTarget / 255.0f - 1.0f};

			if (matchConfidence > highlightThreshold) {
				const float highlightAlpha{(matchConfidence - highlightThreshold)
					/ (1.0f - highlightThreshold)};

				static const unsigned char highlightColor[imageColorChannels]{
					0x33, 0xFF, 0x33};  // Green
				shade_rectangle(
					imageData, imageWidth,
					highlightColor, highlightAlpha,
					tileLeft, tileLeft + tileWidth - 1,
					tileTop, tileTop + tileHeight - 1);
			}
			if (matchConfidence > matchThreshold) {
				++matchCount;
			}

			++tileIndex;
		}
	}

	static const unsigned char borderColor[imageColorChannels]{
		0xFF, 0xFF, 0x00};  // Yellow
	outline_rectangle(
		imageData, imageWidth,
		borderColor,
		gridLeft, gridLeft + tileWidth * gridColumns - 1,
		gridTop, gridTop + tileWidth * gridRows - 1);

	return matchCount;
}


extern "C" void free_results(unsigned int * result)
{
delete[] result;
}

extern "C" void deinit() {
FoldedMVDeinit();
}

extern "C" int main(int argc, char*argv[])
{
	if (argc != 5)
	{
		cout << "4 parameters are needed: " << endl;
		cout << "1 - folder for the binarized weights (binparam-***) - full path " << endl;
		cout << "2 - path to image to be classified" << endl;
		cout << "3 - number of classes in the dataset" << endl;
		cout << "4 - expected result" << endl;
		return 1;
	}
	load_parameters(argv[1]);
	float execution_time = 0;
	unsigned int scores[64];
	unsigned int class_inference = inference(argv[2], scores, atol(argv[3]), &execution_time);
	cout << "Detected class " << class_inference << endl;
	cout << "in " << execution_time << " microseconds" << endl;
	deinit();
	if (class_inference != atol(argv[4]))
		return 1;
	else
		return 0;
}
